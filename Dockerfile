FROM node:10.6

RUN npm install -g yo

RUN adduser --disabled-password --gecos "" --shell /bin/bash yeoman; \
  echo "yeoman ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
ENV HOME /home/yeoman
RUN mkdir -p /home/yeoman/.npm_global \
  && chmod -R 777 /home/yeoman \
  && mkdir -p /usr/src/app \
  && chmod -R 777 /usr/src/app
ENV NPM_CONFIG_PREFIX /home/yeoman/.npm_global

WORKDIR /usr/src/app
USER yeoman
